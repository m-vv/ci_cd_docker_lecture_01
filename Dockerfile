#Dockerfile
#base image from https://hub.docker.com/_/php php 7.4 with apache
FROM php:7.4-apache
# maintainer of
MAINTAINER mvv <vlad.leo@gmail.com>

#src containe all code of application
COPY src/ /var/www/html/
#proper owner of files
RUN chown -R www-data:www-data /var/www/html/