
# Work with pure Dockerfile

You can build and run containers manually.

1. Put code of your application to src directory
2. Goto to project root directory
3. '''docker build -t name_of_your_container .''' command to create your container
4. ''' docker run --rm -it -p 8082:80 name_of_your_container'''
5. In your host system go in any browser open http://localhost:8082 and you will see output from your application.
6. Close your container after work. ''' docker stop id_of_container'''

---

# Working with Docker Hub

1. Address of container in hub docker [https://hub.docker.com/repository/docker/mvvm/simple-docker-hello/general](https://hub.docker.com/repository/docker/mvvm/simple-docker-hello/general).
2. In new system use ''' docker pull mvvm/simple-docker-hello''' to get right image of container.
3. To run application use ''' docker run --rm -it -p 8082:80  mvvm/simple-docker-hello'''
4. In your host system go in any browser open http://localhost:8082 and you will see output from  application.
5. Go back to the **Source** page.

---

## Docker-compose.yml

1. Goto to project root directory.
2. Give command ''' docker-compose up -d'''
3. In your host system go in any browser open http://localhost:8082 and you will see output from  application.
4. Close your container after work. '''docker-compose stop'''
